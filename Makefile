# tinylisp - small C implementation of Lisp

.POSIX:

include config.mk

SRC = tinylisp.c
OBJ = $(SRC:.c=.o)
BLD = tinylisp.x86_64

all: tinylisp run

.c.o:
	$(CC) $(STCFLAGS) -c $<


$(BLD): $(OBJ)
	$(CC) -o $@ $(OBJ) $(STLDFLAGS)

clean:
	rm -f $(BLD) $(OBJ)

run: $(BLD)
	./$(BLD)

.PHONY: all run clean
