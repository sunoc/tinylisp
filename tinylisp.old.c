/* Tiny Lisp with NaN boxing */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Only two types will be used for this Lisp implementation: */
#define I unsigned           /* for integers */
#define L double             /* for Lisp expression */
#define B unsigned long long /* for the boxing system */

/* getting the tag */
#define T(x) *(B*)&x>>48

/* Lisp tags in the NaN boxing system */
I ATOM = 0x7ff8, /* pointer to atom name on the heap */
  PRIM=0x7ff9, /* built in primitives */
  CONS=0x7ffa, /* pointer to the car and cdr pair on the heap */
  CLOS=0x7ffb, /* pointer to closure pairs on the heap */
  NIL=0x7ffc; /* emptly list */

/* Pre-defined atoms (symbols) */
L nil, tru, err;

L cell[N]; /* array containing the heap and the stack */
I hp = 0; /* heap pointer to the next free bytes*/
I sp = N; /* stack pointer to the top value on the stack */

/* Stack / heap related elements */
#define A (char*)cell /* the starting adress of the heap */
#define N 1024 /* size of the heap / stack array */

/* Taking a tab and a value, will return a "box" */
static L
box(I t,I i)
{
  L x;
  *(B*)&x = (B)t<<48|i;
  return x;
}

/* Gives you a values from a "box" */
static I
ord(L x)
{
  return *(B*)&x;
}

/* Wrap function that only returns your expression */
static L
num(L n)
{
  return n;
}

/* Tests if expressions data are equal */
static I
equ(L x,L y)
{
  return *(B*)&x == *(B*)&y;
}

/* Opposit logic value of an expression */
static I
not(L x)
{
  return T(x) == NIL;
}

/* creates a new atom on the heap, with the name s.
   The name of the atom is stored on the heap and the function
   returns the address on the heap boxed with the tag ATOM. */
static L
atom(const char *s)
{
  I i = 0; /* index */
  /* while the index is smaller than the hp
     and if the atome name (s) is not identical */
  while (i < hp && strcmp(A+i,s)) {
    /* we ++ the index */
    i += strlen(A+i)+1;
  }

  /* then, when we reach the hp, we try to copy the string s
     to the A+i address. If it works, we update the hp.
     We also test if the hp value is still under the stack pointer.*/
  if (i == hp && (hp += strlen(strcpy(A+i,s))+1) > sp<<3) abort();

  /* Return the address on the heap tagged as ATOM */
  return box(ATOM, i);
}

/* puts two values as cell on the stack,
 decrease the sp and returns the sp value
 boxed and tagged as CONS.
Cons basically put two values together.
If we want lager list, we can

> (cons 'a (cons 'b  'c))
(a b c)

Also note that if the hp is larger than the newer sp,
it means we are running out of memory. */
static L
cons(L x, L y)
{
  cell[--sp] = x;
  cell[--sp] = y;
  if (hp > sp<<3) abort();
  return box(CONS, sp);
}

/* similarly, if we want the first element of
   a cons, one can simply run the following.
   Note that the ~(CONS^CLOS) is a clever mask
   that allows to accept either CONS or CLOS
   as a tag.
   If the condition is true, we take the value on the stack
   at the address of the cons + 1 (we want the 1st element)*/
static L
car(L p)
{
  return (T(p) & ~(CONS^CLOS)) == CONS ? cell[ord(p)+1] : err;
}

/* same when we want the second value: we only need to get
   the value at the Cons address */
static L
cdr(L p)
{
  return (T(p) & ~(CONS^CLOS)) == CONS ? cell[ord(p)] : err;
}

/* using cons, it is possible to define the pair function.
   v and x are the value in a (v . x) dotted pair, then
   e is the lisp environment. */
static L
pair(L v, L x, L e)
{
  return cons(cons(v, x), e);
}

/* A closure represent an instance of a lambda function. */
static L
closure(L v, L x, L e)
{
  return box(CLOS,ord(pair(v,x,equ(e, env) ? nil : e)));
}

/* Lisp environment list can be search with the assoc function */
static L
assoc(L v, L e)
{
  while(T(e) == CONS && !equ(v, car(e))) e = cdr(e);

  return T(e) == CONS ? cdr(cdr(car(e))) : err;
}

static L
reduce(L f, L t, L e)
{
  return
    eval(cdr(car(f)),
         bind(car(car(f)),
              evlis(t, e),
              not(cdr(f)) ? env : cdr(f)
              )
         );
}

static L
apply(L f, L t, L e)
{
  return
    T(f) == PRIM ? prim[ord(f)].f(t, e) :
    T(f) == CLOS ? reduce(f, t, e) :
    err;
}

static L
evlis(L t, L e)
{

}

static L
bind(L v, L t, L e)
{

}


/* If the type evaluated is an atom, we return it
 with the passed environment.
 If it's a constant, we evaluate both element
 and apply the the environent to them.
 Else, just return the value x.
*/
static L
eval(L x, L e)
{
  return
    T(x) == ATOM ? assoc(x , e) :
    T(x) == CONS ? apply(eval(car(x), e), cdr(x), e) :
    x;
}

int
main()
{

  /* Core Lisp atoms */
  nil = box(NIL, 0);   /* By default, NIL is false */
  tru = atom("#t"); /* #t is an explicit true*/
  err = atom("ERR"); /* Error atom */

  printf("%s\n", A);
  printf("%s\n", A+(strlen(A))+1);

  printf("Number of bitsm for llu: %lu\n", sizeof(B)*8);
  printf("Number of bits for lf: %lu\n", sizeof(L)*8);
  I a = 42;
  L a_boxed = box(PRIM, a);

  I b = 42;
  L b_boxed = box(ATOM, b);

  printf("Tag of a: %llx\n", T(a_boxed));
  printf("Unbox a: %i\n", ord(a_boxed));
  printf("Num of a: %f\n", num(ord(a_boxed)));
  printf("a equal b: %i\n", equ(a_boxed, b_boxed));
  return 0;
}
