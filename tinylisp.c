/* Includes */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Definitions */
/* In order to keep the overall code consise, some */
/* commonly used types are re-defined. */

/* Only two types will be used for this Lisp implementation: */
#define I unsigned           /* for integers */
#define L double             /* for Lisp expression */
#define B unsigned long long /* for the boxing system */


/* getting the tag */
#define T(x) *(B*)&x>>48

/* Stack / heap related elements */
#define A (char*)cell /* the starting adress of the heap */
#define N 1024 /* size of the heap / stack array */

/* Global variables */

/* Lisp tags in the NaN boxing system */
I ATOM = 0x7ff8, /* pointer to atom name on the heap */
  PRIM  = 0x7ff9, /* built in primitives */
  CONS = 0x7ffa, /* pointer to the car and cdr pair on the heap */
  CLOS = 0x7ffb, /* pointer to closure pairs on the heap */
  NIL = 0x7ffc; /* emptly list */

/* Pre-defined atoms (symbols) */
L nil, tru, err;

L cell[N]; /* array containing the heap and the stack */
I hp = 0; /* heap pointer to the next free bytes*/
I sp = N; /* stack pointer to the top value on the stack */

/* Main function */

int
main()
{
  printf("Hello!\r\n");
  return 0;
}
